package com.ssz.dao;

import com.ssz.dto.User;
import com.ssz.util.MyMapper;

import java.util.List;

/**
 * Created by Administrator on 2017/7/22.
 */
public interface UserDao extends MyMapper<User>{
     List<User> getUsers();
}
