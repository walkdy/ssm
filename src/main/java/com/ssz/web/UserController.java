package com.ssz.web;

import com.ssz.dto.User;
import com.ssz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService i_userService;

	@RequestMapping(value = "/users",method = RequestMethod.GET)
	public String users(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		List<User> users =i_userService.getUsers();
		model.addAttribute("users",users);
		return "user/user_list";
	}
	@RequestMapping(value = "/users",method = RequestMethod.POST)
	public String insert(User user,ModelMap model) {
		i_userService.insert(user);
		List<User> users =i_userService.getUsers();
		model.addAttribute("users", users);
		return "user/user_list";
	}
}