package com.ssz.service.impl;

import com.ssz.dao.UserDao;
import com.ssz.dto.User;
import com.ssz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2017/7/22.
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    public List<User> getUsers() {
        System.out.println("---");
        return userDao.getUsers();
    }

    @Transactional(propagation= Propagation.REQUIRED)//��������
    public int insert(User user) {
        return userDao.insert(user);
    }
}
