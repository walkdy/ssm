package com.ssz.service;

import com.ssz.dto.User;
import java.util.List;

/**
 * Created by Administrator on 2017/7/22.
 */
public interface UserService {
     List<User> getUsers();
     int insert(User user);
}
